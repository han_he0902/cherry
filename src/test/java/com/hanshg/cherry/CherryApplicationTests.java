package com.hanshg.cherry;

import com.hanshg.cherry.config.aliyun.AliyunConfig;
import com.hanshg.cherry.service.UserService;
import com.hanshg.cherry.service.aliyun.AliYunOssService;
import com.hanshg.cherry.service.mail.MailService;
import com.hanshg.cherry.util.DateUtils;
import com.hanshg.cherry.util.redis.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootTest
@Slf4j
class CherryApplicationTests {

    @Autowired
    private AliyunConfig aliyunConfig;

    @Autowired
    private MailService mailService;

    @Autowired
    private AliYunOssService aliYunOssService;

    @Autowired
    private UserService userService;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private RedisUtil redisUtil;

    /**
     * 测试密码加密
     */
    @Test
    void contextLoads() {
        //System.out.println(new BCryptPasswordEncoder().encode("user"));
    }

    @Test
    void redisByName() {
//        redisUtil.flushDB();
//        redisUtil.set("name", 38);
//       System.out.println(redisUtil.get(Constants.LOGIN_VERIFY_SESSION));
    }

    @Test
    void dateTest(){
        System.out.println(DateUtils.getDate());
    }

}
