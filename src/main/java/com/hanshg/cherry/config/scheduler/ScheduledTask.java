package com.hanshg.cherry.config.scheduler;

import java.util.concurrent.ScheduledFuture;

/**
 * @ClassName ScheduledTask
 * @Description ScheduledFuture的包装类。ScheduledFuture是ScheduledExecutorService定时任务线程池的执行结果
 * @Author 柠檬水
 * @Date 2020/4/20 11:14
 * @Version 1.0
 **/
public final class ScheduledTask {

    volatile ScheduledFuture<?> future;

    /**
     * 取消定时任务
     */
    public void cancel() {
        ScheduledFuture<?> future = this.future;
        if (future != null) {
            future.cancel(true);
        }
    }
}
