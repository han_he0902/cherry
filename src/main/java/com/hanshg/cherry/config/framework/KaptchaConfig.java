package com.hanshg.cherry.config.framework;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * @ClassName KaptchaConfig
 * @Description 验证码生成器
 * @Author 柠檬水
 * @Date 2020/4/11 15:24
 * @Version 1.0
 **/
@Configuration
public class KaptchaConfig {

    @Bean
    public DefaultKaptcha defaultKaptcha() {
        DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
        Properties properties = new Properties();
        // 图片边框，默认有
        properties.setProperty(Constants.KAPTCHA_BORDER, "no");
        // 边框颜色，默认黑色
        //properties.setProperty(Constants.KAPTCHA_BORDER_COLOR, "105,179,90");
        // 干扰线颜色，默认黑色
        //properties.setProperty(Constants.KAPTCHA_NOISE_COLOR, "blank");
        // 干扰线实现类
        //properties.setProperty(Constants.KAPTCHA_NOISE_IMPL, "com.google.code.kaptcha.impl.DefaultNoise");
        // 文本集合，验证码值从此集合中拿
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_STRING, "23456789abcefghjkmnpqrstuvwxyz");
        // 验证码长度
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_LENGTH, "4");
        // 文本间隔
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_SPACE, "8");
        // 字体
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_FONT_NAMES, "宋体,楷体,微软雅黑");
        // 字体颜色，默认黑色
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_FONT_COLOR, "yellow");
        // 字体大小
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_FONT_SIZE, "30");
        // 背景颜色渐变开始色
        properties.setProperty(Constants.KAPTCHA_BACKGROUND_CLR_FROM, "196,196,196");
        // 背景颜色渐变结束色，默认白色
        properties.setProperty(Constants.KAPTCHA_BACKGROUND_CLR_TO, "white");
        // 图片宽度
        properties.setProperty(Constants.KAPTCHA_IMAGE_WIDTH, "120");
        // 图片高度
        properties.setProperty(Constants.KAPTCHA_IMAGE_HEIGHT, "50");
        // 图片样式
        properties.setProperty(Constants.KAPTCHA_OBSCURIFICATOR_IMPL, "com.google.code.kaptcha.impl.ShadowGimpy");

        Config config = new Config(properties);
        defaultKaptcha.setConfig(config);
        return defaultKaptcha;
    }
}
