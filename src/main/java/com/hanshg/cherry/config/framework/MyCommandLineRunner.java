package com.hanshg.cherry.config.framework;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 启动成功打印
 * @author 柠檬水
 */
@Component
@Order(value = 1)
public class MyCommandLineRunner implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        //启动成功后打印
        System.out.println("");
        System.out.println("                   (♥◠‿◠)ﾉﾞ  樱桃启动成功   ლ(´ڡ`ლ)ﾞ  \n");
    }
}
