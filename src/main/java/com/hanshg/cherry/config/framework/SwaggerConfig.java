package com.hanshg.cherry.config.framework;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author 柠檬水
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket docket(Environment environment) {

        //检测是否为开发环境，生产环境不进行展示swagger
        Profiles profiles = Profiles.of("dev","test");
        boolean flag = environment.acceptsProfiles(profiles);

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .enable(flag)
                .groupName("柠檬水")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.hanshg.cherry.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        Contact contact = new Contact("柠檬水", "http://www.cherryok.cn", "hanshouge@163.com");
        return new ApiInfoBuilder()
                .title("Cherry 接口 API 文档")
                .description("<div style='font-size:18px;color:red;'>Cherry System RestFul API</div>")
                .contact(contact)
                .termsOfServiceUrl("http://www.cherryok.cn")
                .version("1.2")
                .build();
    }
}



