package com.hanshg.cherry.config.aliyun;

import com.aliyun.oss.OSSClientBuilder;
import com.hanshg.cherry.model.SysOss;

/**
 * @ClassName AliyunCloudStorageService
 * @Description TODO
 * @Author 柠檬水
 * @Date 2020/4/16 15:37
 * @Version 1.0
 **/
public class AliyunCloudStorageService extends CloudStorageService {


    public AliyunCloudStorageService(SysOss sysOss) {
        this.sysOss = sysOss;
        //初始化
        init();
    }

    private void init() {
        if (this.ossClient == null) {
            synchronized (AliyunCloudStorageService.class) {
                if (this.ossClient == null) {
                    this.ossClient = new OSSClientBuilder().build(sysOss.getEndPoint(), sysOss.getAccessKeyId(), sysOss.getAccessKeySecret());
                }
            }
        }
    }

}
