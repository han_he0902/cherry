package com.hanshg.cherry.config.aliyun;

import com.aliyun.oss.OSS;
import com.hanshg.cherry.model.SysOss;

/**
 * @ClassName CloudStorageService
 * @Description TODO
 * @Author 柠檬水
 * @Date 2020/4/16 15:36
 * @Version 1.0
 **/
public abstract class CloudStorageService {

    public SysOss sysOss;

    public OSS ossClient;
}
