package com.hanshg.cherry.config.aliyun;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @ClassName SmsConfig
 * @Description 阿里云短信服务
 * @Author 柠檬水
 * @Date 2020/4/18 15:53
 * @Version 1.0
 **/
@Component
@ConfigurationProperties(prefix = "aliyun.sms")
public class SmsConfig {

    /**
     * 阿里云accessKeyId
     */
    private String accessKeyId;

    /**
     * 阿里云accessKeySecret
     */
    private String accessKeySecret;

    /**
     * 签名管理中的签名
     */
    private String sign_name;

    /**
     * 模板管理中的模板CODE
     */
    private String template_code;

    /**
     * 标识是否开启短信发送
     */
    private String flag;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getAccessKeySecret() {
        return accessKeySecret;
    }

    public void setAccessKeySecret(String accessKeySecret) {
        this.accessKeySecret = accessKeySecret;
    }

    public String getSign_name() {
        return sign_name;
    }

    public void setSign_name(String sign_name) {
        this.sign_name = sign_name;
    }

    public String getTemplate_code() {
        return template_code;
    }

    public void setTemplate_code(String template_code) {
        this.template_code = template_code;
    }
}
