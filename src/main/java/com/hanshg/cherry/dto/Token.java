package com.hanshg.cherry.dto;

import lombok.Data;
import java.io.Serializable;

/**
 * Restful方式登陆token
 * @author 柠檬水
 */
@Data
public class Token implements Serializable {

	private String token;
	/** 登陆时间戳（毫秒） */
	private Long loginTime;

	public Token(String token, Long loginTime) {
		super();
		this.token = token;
		this.loginTime = loginTime;
	}
}
