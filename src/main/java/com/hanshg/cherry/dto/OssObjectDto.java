package com.hanshg.cherry.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * ossobject dto
 *
 * @ClassName OssObjectDto
 * @Description 操作对象
 * @Author 柠檬水
 * @Date 2020/4/14 20:56
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OssObjectDto implements Serializable {

    /** The name of the bucket in which this object is stored */
    private String bucketName;

    /** The key under which this object is stored */
    private String key;

    private String size;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastModified;

    private String storageClass;
}
