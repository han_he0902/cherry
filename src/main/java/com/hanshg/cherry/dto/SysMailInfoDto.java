package com.hanshg.cherry.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hanshg.cherry.model.SysMailInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author 柠檬水
 */
public class SysMailInfoDto extends SysMailInfo {

    @JsonIgnore
    private MultipartFile[] multipartFiles;

    public MultipartFile[] getMultipartFiles() {
        return multipartFiles;
    }

    public void setMultipartFiles(MultipartFile[] multipartFiles) {
        this.multipartFiles = multipartFiles;
    }
}
