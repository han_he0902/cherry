package com.hanshg.cherry.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 柠檬水
 */
@Data
public class BeanField implements Serializable {

	private String columnName;

	private String columnType;

	private String columnComment;

	private String columnDefault;

	private String name;

	private String type;

}
