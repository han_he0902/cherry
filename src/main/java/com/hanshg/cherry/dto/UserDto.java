package com.hanshg.cherry.dto;

import com.hanshg.cherry.model.SysUser;

/**
 * 用户dto
 *
 * @author 柠檬水
 * @date 2020/04/18
 */
public class UserDto extends SysUser {

    /**
     * 短信代码
     */
    private String smsCode;

    public String getSmsCode() {
        return smsCode;
    }

    public void setSmsCode(String smsCode) {
        this.smsCode = smsCode;
    }
}
