package com.hanshg.cherry;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * 程序启动类
 * @author 柠檬水
 * EnableSwagger2 开启文档
 * EnableAsync  开启异步
 * EnableScheduling 开启任务调度
 */
@SpringBootApplication
@ServletComponentScan
public class CherryApplication {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(CherryApplication.class);
        //打印横幅图形
        springApplication.setBannerMode(Banner.Mode.CONSOLE);
        springApplication.run(args);
    }

}
