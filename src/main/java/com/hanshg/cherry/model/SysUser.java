package com.hanshg.cherry.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.hanshg.cherry.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 柠檬水
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "用户表")
public class SysUser extends BaseEntity<Long> {

    private String username;
    private String password;
    @ApiModelProperty(value = "性别",example = "0")
    private Integer sex;
    @ApiModelProperty(value = "年龄",example = "32")
    private Integer age;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;
    private String nickname;
    private String photo;
    private String telephone;
    private String email;
    @ApiModelProperty(value = "状态",example = "1")
    private Integer status;

    public interface Status{
      int DISABLED = 0; //无效
      int VALID = 1; //已启动
      int LOCKED = 2; //已锁定
  }
}
