package com.hanshg.cherry.model;

import com.hanshg.cherry.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author 柠檬水
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "文件表")
public class SysFile extends BaseEntity<Long> {

    private String title;
    private String fileName;
    private String pathUrl;
    private String visitUrl;
    private String sizeValue;
    @ApiModelProperty(value = "状态", example = "1")
    private Integer status;

    public interface Status {
        int DISABLED = 0;
        int VALID = 1;
    }

}
