package com.hanshg.cherry.model;

import com.hanshg.cherry.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @ClassName SysJob
 * @Description TODO
 * @Author 柠檬水
 * @Date 2020/4/20 11:19
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "定时任务")
public class SysJob extends BaseEntity<Long> {

    /**
     * bean名称
     */
    private String beanName;
    /**
     * 方法名称
     */
    private String methodName;
    /**
     * 方法参数
     */
    private String methodParam;
    /**
     * cron表达式
     */
    private String cron;
    /**
     * 状态（1正常 0暂停）
     */
    private Integer status;
    /**
     * 备注
     */
    private String remark;

    public interface Status{
        int NORMAL = 1; //正常
        int PAUSE = 2; //禁用
    }
}
