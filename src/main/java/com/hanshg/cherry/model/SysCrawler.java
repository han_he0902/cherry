package com.hanshg.cherry.model;

import com.hanshg.cherry.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @ClassName SysRecruit
 * @Description TODO
 * @Author 柠檬水
 * @Date 2020/4/23 11:44
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "招聘信息")
public class SysCrawler extends BaseEntity<Long> {

    private String companyName;
    private String companyAddr;
    private String companyInfo;
    private String jobName;
    private String jobAddr;
    private String jobInfo;
    private Integer salaryMin;
    private Integer salaryMax;
    private String education;
    private String work;
    private String recruit;
    private String url;
    private String time;

}
