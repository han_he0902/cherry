package com.hanshg.cherry.model;

import com.hanshg.cherry.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author 柠檬水
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "用户角色表")
public class SysRoleUser extends BaseEntity<Long> {

  @ApiModelProperty(value = "用户id",example = "123")
  private Long userId;

  @ApiModelProperty(value = "角色id",example = "1")
  private Long roleId;

}
