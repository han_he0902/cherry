package com.hanshg.cherry.model;

import com.hanshg.cherry.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author 柠檬水
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "新闻信息")
public class SysNews extends BaseEntity<Long> {

	private Long typeId;
	private Integer imgType;
	private String coverImg;
	private String category;
	private String publisher;
	private String publisherFace;
	private String title;
	private String content;

}
