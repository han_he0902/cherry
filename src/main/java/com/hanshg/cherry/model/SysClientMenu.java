package com.hanshg.cherry.model;

import com.hanshg.cherry.model.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author 柠檬水
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SysClientMenu extends BaseEntity<Long> {

    private String title;

    private String href;

    private Integer sort;

    private Integer status;

}