package com.hanshg.cherry.controller.client;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.hanshg.cherry.base.result.PageTableRequest;
import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.model.SysClientProduct;
import com.hanshg.cherry.service.client.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 产品控制器
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@Controller
@RequestMapping("client/product")
@Slf4j
@Api(tags = "产品控制层")
public class ProductController {

    @Autowired
    private ProductService productService;

    /**
     * 保存或更新
     *
     * @param clientProduct 客户端产品
     * @return {@link Results}
     */
    @RequestMapping(value = "/saveOrUpdate", method = RequestMethod.POST)
    @ApiOperation(value = "保存更新")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    @ResponseBody
    public Results saveOrUpdate(SysClientProduct clientProduct) {
        if (null != clientProduct.getId() && 0 != clientProduct.getId()) {
            return productService.updateProduct(clientProduct);
        }
        return productService.saveProduct(clientProduct);
    }

    /**
     * 所有产品
     *
     * @return {@link List<SysClientProduct>}
     */
    @GetMapping("/allProduct")
    @ApiOperation(value = "查询全部")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    @ResponseBody
    public List<SysClientProduct> allProduct(){
        return productService.allProduct();
    }

    /**
     * 列表
     *
     * @param request 请求
     * @return {@link Results<SysClientProduct>}
     */
    @GetMapping("/listPage")
    @ApiOperation(value = "分页列表列表")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    @ResponseBody
    public Results<SysClientProduct> list(PageTableRequest request) {
        Results<SysClientProduct> list = productService.getProductByPage(request.getPage(), request.getLimit());
        return list;
    }

    /**
     * 列表页
     *
     * @param request       请求
     * @param clientProduct 客户端产品
     * @return {@link Results<SysClientProduct>}
     */
    @GetMapping("/listByPage")
    @ApiOperation(value = "模糊查询列表")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    @ResponseBody
    public Results<SysClientProduct> listByPage(PageTableRequest request, SysClientProduct clientProduct) {
        return productService.getProductByPage(request.getPage(), request.getLimit(), clientProduct);
    }

    /**
     * 删除
     *
     * @param id id
     * @return {@link Results}
     */
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    @ApiOperation(value = "删除")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    @ResponseBody
    public Results delete(Long id) {
        SysClientProduct product = productService.getSysClientProductById(id);
        //更新状态为无效
        product.setStatus(0);
        productService.updateProduct(product);
        return productService.deleteProduct(id);
    }

    /**
     * 角色编辑
     *
     * @param model         模型
     * @param clientProduct 客户端产品
     * @return {@link ModelAndView}
     */
    @ApiOperation(value = "编辑页面", notes = "跳转到菜单信息编辑页面")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    @RequestMapping(value = "/addOrEdit", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView roleEdit(Model model, SysClientProduct clientProduct) {
        SysClientProduct newSysClientProduct = new SysClientProduct();
        if (0 != clientProduct.getId()) {
            newSysClientProduct = productService.getSysClientProductById(clientProduct.getId());
        }
        model.addAttribute("clientProduct", newSysClientProduct);
        ModelAndView modelAndView = new ModelAndView("manage/product-add");
        return modelAndView;
    }

    /**
     * 回收
     *
     * @return {@link String}
     */
    @ApiOperation(value = "回收站", notes = "跳转到回收站页面")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    @GetMapping("/recycle")
    public String recycle(){
        return "manage/product-recycle";
    }

    /**
     * 回收列表
     *
     * @return {@link Results<SysClientProduct>}
     */
    @GetMapping("/recycleList")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    @ResponseBody
    public Results<SysClientProduct> recycleList() {
        return productService.recycleList();
    }

    /**
     * 删除回收
     *
     * @param clientProduct 客户端产品
     * @return {@link Results}
     */
    @GetMapping("/deleteRecycle")
    @ResponseBody
    @ApiOperation(value = "删除回收站信息", notes = "删除回收站信息")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    public Results deleteRecycle(SysClientProduct clientProduct) {
        int count = productService.deleteRecycle(clientProduct.getId());
        if (count > 0) {
            return Results.success();
        } else {
            return Results.failure();
        }
    }

    /**
     * 减少
     *
     * @param clientProduct 客户端产品
     * @return {@link Results}
     */
    @GetMapping("/reduce")
    @ResponseBody
    @ApiOperation(value = "还原信息", notes = "还原回收站信息")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    public Results reduce(SysClientProduct clientProduct) {
        return productService.reduceProduct(clientProduct);
    }
}
