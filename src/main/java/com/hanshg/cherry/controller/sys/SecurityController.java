package com.hanshg.cherry.controller.sys;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 安全控制器
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@Controller
@Api(tags = "安全认证层")
public class SecurityController implements ErrorController {

    @Autowired
    private HttpServletRequest request;

    @GetMapping("/login.html")
    public String login() {
        return "login";
    }

    @GetMapping("/user-register.html")
    public String register() {
        return "user-register";
    }

    @GetMapping("/403.html")
    public String noPermission() {
        return "error/403";
    }

    @Override
    @RequestMapping("/error")
    public String getErrorPath() {
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        switch (statusCode) {
            case 404:
                return "error/404";
            case 400:
                return "error/403";
            default:
                return "error/500";
        }
    }
}
