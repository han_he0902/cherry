package com.hanshg.cherry.controller.druid;

import com.alibaba.druid.stat.DruidStatManagerFacade;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 德鲁伊stat控制器
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@RestController
@Slf4j
@Api(tags = "数据源监控层")
public class DruidStatController {

    /**
     * 德鲁伊的统计
     *
     * @return {@link Object}
     */
    @GetMapping("/druid/stat")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    public Object druidStat() {
        // DruidStatManagerFacade#getDataSourceStatDataList 该方法可以获取所有数据源的监控数据，除此之外 DruidStatManagerFacade 还提供了一些其他方法，你可以按需选择使用。
        return DruidStatManagerFacade.getInstance().getDataSourceStatDataList();
    }

}
