package com.hanshg.cherry.controller.log;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.hanshg.cherry.base.result.PageTableRequest;
import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.model.SysErrorLog;
import com.hanshg.cherry.service.log.ErrorLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日志控制器
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@Controller
@RequestMapping("errorLog")
@Slf4j
@Api(tags = "异常日志控制层")
public class ErrorLogController {

    private final ErrorLogService errorLogService;

    public ErrorLogController(ErrorLogService errorLogService) {
        this.errorLogService = errorLogService;
    }

    @GetMapping("/list")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:error:query')")
    @ApiOperation(value = "分页获取异常日志信息", notes = "分页获取异常日志信息")//描述
    @ApiImplicitParam(name = "request", value = "分页查询异常实体类")
    public Results<SysErrorLog> getLogs(PageTableRequest request) {
        log.info("ErrorLogController.getLogs():param (page=" + request + ")");
        Page<SysErrorLog> page = new Page<>(request.getPage(), request.getLimit());
        IPage<SysErrorLog> jobPage = errorLogService.page(page);
        return Results.success((int) jobPage.getTotal(), jobPage.getRecords());
    }

    /**
     * 处理日期转换
     */
    String pattern = "yyyy-MM-dd";

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat(pattern), true));
    }

    @GetMapping("/findLog")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:error:query')")
    @ApiOperation(value = "模糊查询异常日志信息", notes = "模糊搜索查询异常日志信息")//描述
    @ApiImplicitParams({
            @ApiImplicitParam(name = "methodName", value = "模糊搜索的方法名称", required = true),
    })
    public Results<SysErrorLog> findLogByUsername(PageTableRequest request, SysErrorLog errorLog) {
        log.info("ErrorLogController.findLogByUsername():param (page=" + request + "+" + errorLog.getMethodName() + ")");
        Page<SysErrorLog> page = new Page<>(request.getPage(), request.getLimit());
        QueryWrapper<SysErrorLog> wrapper = new QueryWrapper<>();
        wrapper.like("method_name", errorLog.getMethodName());
        IPage<SysErrorLog> jobPage = errorLogService.page(page,wrapper);
        return Results.success((int) jobPage.getTotal(), jobPage.getRecords());
    }


    /**
     * 细节
     *
     * @param model    模型
     * @param errorLog 错误日志
     * @return {@link String}
     */
    @GetMapping(value = "/detail")
    @ApiOperation(value = "查询异常信息")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    public String detail(Model model,SysErrorLog errorLog) {
        model.addAttribute("errorLog", errorLogService.getById(errorLog.getId()));
        return "log/detail";
    }
}
