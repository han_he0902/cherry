package com.hanshg.cherry.controller.api;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * api控制器
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@Controller
@RequestMapping("${api-url}")
@Api(tags = "API公共访问层")
public class ApiController {

    /**
     * 获取页面
     *
     * @param modelAndView 模型和视图
     * @param pageName     页面名称
     * @return {@link ModelAndView}
     */
    @RequestMapping("/getPage")
    @ApiOperation(value = "公共接口")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    public ModelAndView getPage(ModelAndView modelAndView,String pageName){
        modelAndView.setViewName(pageName);
        return modelAndView;
    }
}
