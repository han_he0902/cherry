package com.hanshg.cherry.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.hanshg.cherry.base.core.Constants;
import com.hanshg.cherry.base.result.ResponseCode;
import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.dto.UserDto;
import com.hanshg.cherry.model.SysUser;
import com.hanshg.cherry.service.UserService;
import com.hanshg.cherry.util.StringUtils;
import com.hanshg.cherry.util.redis.RedisUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author 柠檬水
 */
@Controller
@Slf4j
@RequestMapping("register")
@Api(tags = "用户注册")
public class RegisterController {

    private final UserService userService;

    private final RedisUtil redisUtil;

    public RegisterController(UserService userService, RedisUtil redisUtil) {
        this.userService = userService;
        this.redisUtil = redisUtil;
    }

    /**
     * 注册
     *
     * @param userDto 用户dto
     * @return {@link Results<SysUser>}
     */
    @PostMapping(value = "/reg")
    @ResponseBody
    @ApiOperation(value = "注册用户信息", notes = "注册用户信息")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    public Results<SysUser> reg(UserDto userDto) {
        log.info("UserController.register():param (" + userDto + ")");
        SysUser sysUser;
        sysUser = userService.getUser(userDto.getUsername());
        if (sysUser != null && !(sysUser.getId().equals(userDto.getId()))) {
            return Results.failure(ResponseCode.USERNAME_REPEAT.getCode(), ResponseCode.USERNAME_REPEAT.getMessage());
        }
        sysUser = userService.getUserByPhone(userDto.getTelephone());
        if (sysUser != null && !(sysUser.getId().equals(userDto.getId()))) {
            return Results.failure(ResponseCode.PHONE_REPEAT.getCode(), ResponseCode.PHONE_REPEAT.getMessage());
        }
        //获取短信验证码
        String smsCode = userDto.getSmsCode();

        //获取redis中短信验证码
        String phoneCode = (String) redisUtil.get(Constants.CHECK_CODE + userDto.getTelephone());
        if (StringUtils.isEmpty(smsCode) || StringUtils.isEmpty(phoneCode) || !smsCode.toLowerCase().equals(phoneCode.toLowerCase())) {
            return Results.failure(ResponseCode.VERIFY_CODE.getCode(), ResponseCode.VERIFY_CODE.getMessage());
        }
        userDto.setStatus(1);
        userDto.setStatus(1);
        userDto.setAge(18);
        //密码加密
        userDto.setPassword(new BCryptPasswordEncoder().encode(userDto.getPassword().trim()));
        return userService.save(userDto, 10L);
    }

    @PostMapping(value = "/sendSms/{mobile}")
    @ResponseBody
    @ApiOperation(value = "获取短信验证码", notes = "获取短信验证码")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    public JSONObject sendSms(@PathVariable String mobile) {
        return userService.sendSms(mobile);
    }
}
