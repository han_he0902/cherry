package com.hanshg.cherry.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.hanshg.cherry.base.result.PageTableRequest;
import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.config.target.Logger;
import com.hanshg.cherry.dto.RoleDto;
import com.hanshg.cherry.model.SysRole;
import com.hanshg.cherry.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 柠檬水
 */
@Controller
@RequestMapping("role")
@Slf4j
@Api(tags = "角色控制层")
public class RoleController {

    @Autowired
    private RoleService roleService;

    /**
     * 把所有角色
     *
     * @return {@link Results<SysRole>}
     */
    @GetMapping("/all")
    @ResponseBody
    @ApiOperation(value = "获取所有角色", notes = "获取所有角色信息")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    public Results<SysRole> getAllRoles() {
        log.info("RoleController.getAllRoles()");
        return roleService.getAllRoles();
    }

    /**
     * 得到的角色
     *
     * @param page 页面
     * @return {@link Results<SysRole>}
     */
    @GetMapping("/list")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:role:query')")
    @ApiOperation(value = "分页获取角色", notes = "用户分页获取角色信息")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "limit", required = true,dataType = "Integer"),
    })
    @Logger(value = "角色查询",type = 1)
    public Results<SysRole> getRoles(PageTableRequest page) {
        log.info("RoleController.getRoles():param (page="+page+")");
        page.countOffset();//计算
        return roleService.getAllRolesByPage(page.getOffset(),page.getLimit());
    }

    /**
     * 添加角色
     *
     * @param model 模型
     * @return {@link String}
     */
    @GetMapping(value = "/add")
    @PreAuthorize("hasAuthority('sys:role:add')")
    @ApiOperation(value = "新增角色信息页面", notes = "跳转到角色信息新增页面")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    @Logger(value = "新增角色",type = 2)
    public String addRole(Model model) {
        model.addAttribute("sysRole",new SysRole());
        return "role/role-add";
    }

    /**
     * 保存角色
     *
     * @param roleDto 角色dto
     * @return {@link Results}
     */
    @PostMapping(value = "/add")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:role:add')")
    @ApiOperation(value = "保存角色信息", notes = "保存新增的角色信息")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    @ApiImplicitParam(name = "roleDto",value = "角色信息实体类", required = true,dataType = "RoleDto")
    public Results saveRole(@RequestBody RoleDto roleDto) {
        return roleService.save(roleDto);
    }

    /**
     * 编辑的角色
     *
     * @param model 模型
     * @param role  角色
     * @return {@link String}
     */
    @GetMapping(value = "/edit")
    @ApiOperation(value = "编辑角色信息页面", notes = "跳转到角色信息编辑页面")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    @ApiImplicitParam(name = "role",value = "角色信息实体类", required = true,dataType = "SysRole")
    @Logger(value = "编辑角色",type = 2)
    public String editRole(Model model, SysRole role) {
        model.addAttribute("sysRole",roleService.getRoleById(role.getId()));
        return "role/role-edit";
    }

    /**
     * 更新的作用
     *
     * @param roleDto 角色dto
     * @return {@link Results}
     */
    @PostMapping(value = "/edit")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:role:edit')")
    @ApiOperation(value = "保存角色信息", notes = "保存被编辑的角色信息")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    @ApiImplicitParam(name = "roleDto",value = "角色信息实体类", required = true,dataType = "RoleDto")
    public Results updateRole(@RequestBody RoleDto roleDto) {
        return roleService.update(roleDto);
    }

    /**
     * 删除角色
     *
     * @param roleDto 角色dto
     * @return {@link Results<SysRole>}
     */
    @GetMapping(value = "/delete")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:role:del')")
    @ApiOperation(value = "删除角色信息", notes = "删除角色信息")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    @Logger(value = "删除角色",type = 2)
    public Results<SysRole> deleteRole(RoleDto roleDto) {
        return roleService.delete(roleDto.getId());
    }

    String pattern = "yyyy-MM-dd";

    /**
     * init粘结剂
     * 只需要加上下面这段即可，注意不能忘记注解
     *
     * @param binder  粘结剂
     * @param request 请求
     */
    @InitBinder
    public void initBinder(WebDataBinder binder, WebRequest request) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat(pattern), true));
    }

    /**
     * 找到角色的角色名称
     *
     * @param requests 请求
     * @param roleName 角色名
     * @return {@link Results}
     */
    @GetMapping("/findRoleByRoleName")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:role:query')")
    @ApiOperation(value = "模糊查询角色信息", notes = "模糊搜索查询角色信息")
    @ApiOperationSupport(author = "柠檬水 cherryok@126.com")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "roleName",value = "模糊搜索的角色名", required = true),
    })
    @Logger(value = "搜索查询角色",type = 2)
    public Results findRoleByRoleName(PageTableRequest requests, String roleName) {
        requests.countOffset();
        return roleService.getRoleByFuzzyRoleNamePage(roleName,requests.getOffset(),requests.getLimit());
    }
}
