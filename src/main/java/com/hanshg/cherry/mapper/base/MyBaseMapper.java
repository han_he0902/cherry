package com.hanshg.cherry.mapper.base;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @ClassName MyBaseMapper
 * @Description 主类
 * @Author 柠檬水
 * @Date 2020/4/14 20:59
 * @Version 1.0
 **/
public interface MyBaseMapper<T> extends BaseMapper<T> {

}
