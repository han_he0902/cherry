package com.hanshg.cherry.mapper.client;

import com.hanshg.cherry.mapper.base.MyBaseMapper;
import com.hanshg.cherry.model.SysClientMenu;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 客户端映射器
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@Mapper
public interface ClientMapper extends MyBaseMapper<SysClientMenu> {

    /**
     * 回收列表
     *
     * @return {@link List<SysClientMenu>}
     */
    @Select("select * from sys_client_menu t where t.deleted = 1 ")
    List<SysClientMenu> recycleList();

    /**
     * 删除和平
     *
     * @param id id
     * @return {@link Integer}
     */
    @Delete("delete from sys_client_menu t where t.id = #{id}")
    Integer deletePeace(Long id);

    /**
     * 减少菜单
     *
     * @param id id
     * @return {@link Integer}
     */
    @Update("update sys_client_menu t set t.status = 1,t.deleted = 0 where t.id = #{id}")
    Integer reduceMenu(Long id);
}
