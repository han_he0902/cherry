package com.hanshg.cherry.mapper.wx;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hanshg.cherry.model.SysNews;
import org.apache.ibatis.annotations.Mapper;

/**
 * 消息映射器
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@Mapper
public interface NewsMapper extends BaseMapper<SysNews> {

}
