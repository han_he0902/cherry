package com.hanshg.cherry.mapper.wx;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hanshg.cherry.model.SysType;
import org.apache.ibatis.annotations.Mapper;

/**
 * 类型映射器
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@Mapper
public interface TypeMapper extends BaseMapper<SysType> {

}
