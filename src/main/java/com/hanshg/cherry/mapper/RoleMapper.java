package com.hanshg.cherry.mapper;

import com.hanshg.cherry.mapper.base.MyBaseMapper;
import com.hanshg.cherry.model.SysRole;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 角色映射器
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@Mapper
public interface RoleMapper extends MyBaseMapper<SysRole> {

    /**
     * 得到的角色
     *
     * @return {@link List<SysRole>}
     */
    @Select("select * from sys_role t")
    List<SysRole> getRoles();

    /**
     * 计算所有角色
     *
     * @return {@link Integer}
     */
    @Select("select count(*) from sys_role t")
    Integer countAllRoles();

    /**
     * 把所有角色的页面
     *
     * @param startPosition 起始位置
     * @param limit         限制
     * @return {@link List<SysRole>}
     */
    @Select("select * from sys_role t limit #{startPosition}, #{limit}")
    List<SysRole> getAllRolesByPage(@Param("startPosition")Integer startPosition, @Param("limit")Integer limit);

    /**
     * 保存
     *
     * @param role 角色
     * @return {@link Integer}
     */
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into sys_role(name, description, create_time, update_time) values(#{name}, #{description}, now(), now())")
    Integer save(SysRole role);

    /**
     * 保存角色
     *
     * @param role 角色
     * @return {@link Integer}
     */
    Integer saveRole(SysRole role);

    /**
     * 通过id
     *
     * @param id id
     * @return {@link SysRole}
     */
    @Select("select * from sys_role t where t.id = #{id}")
    SysRole getById(Long id);

    /**
     * 更新的作用
     *
     * @param role 角色
     * @return {@link Integer}
     */
    Integer updateRole(SysRole role);

    /**
     * 删除角色
     *
     * @param id id
     * @return {@link Integer}
     */
    @Delete("delete from sys_role where id = #{id}")
    Integer deleteRole(Long id);

    /**
     * 数的作用模糊角色名
     *
     * @param roleName 角色名
     * @return {@link Integer}
     */
    @Select("select count(*) from sys_role t where t.name like '%${roleName}%'")
    Integer countRoleByFuzzyRoleName(@Param("roleName") String roleName);

    /**
     * 被模糊的角色角色名称页面
     *
     * @param roleName      角色名
     * @param startPosition 起始位置
     * @param limit         限制
     * @return {@link List<SysRole>}
     */
    @Select("select * from sys_role t where t.name like '%${roleName}%' limit #{startPosition},#{limit}")
    List<SysRole> getRoleByFuzzyRoleNamePage(@Param("roleName") String roleName,@Param("startPosition")Integer startPosition,@Param("limit")Integer limit);
}
