package com.hanshg.cherry.mapper.log;

import com.hanshg.cherry.mapper.base.MyBaseMapper;
import com.hanshg.cherry.model.SysLog;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 日志映射器
 *
 * @author 柠檬水
 * @date 2020/04/12
 */
@Mapper
public interface LogMapper extends MyBaseMapper<SysLog> {

    /**
     * 保存日志
     *
     * @param sysLog 系统日志
     * @return {@link Integer}
     */
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into sys_log(username,operation,os_name,border_name,type,time,method, ip,create_time,params) values(#{username},#{operation},#{osName},#{borderName},#{type}, now(),#{method}, #{ip},now(),#{params})")
    Integer saveLog(SysLog sysLog);

    /**
     * 统计日志
     *
     * @param type     类型
     * @param typeMenu 式菜单
     * @return {@link Integer}
     */
    @Select("select count(*) from sys_log t where t.type in (#{type} , #{typeMenu})")
    Integer countLog(Integer type, Integer typeMenu);

    /**
     * 得到所有日志页面
     *
     * @param type     类型
     * @param page     页面
     * @param limit    限制
     * @param typeMenu 式菜单
     * @return {@link List<SysLog>}
     */
    @Select("select * from sys_log t where t.type in (#{type} , #{typeMenu}) order by t.id desc limit #{page},#{limit} ")
    List<SysLog> getAllLogByPage(Integer type, Integer typeMenu, @Param("page") Integer page, @Param("limit") Integer limit);

    /**
     * 发现日志的用户名
     *
     * @param username 用户名
     * @param type     类型
     * @return {@link Integer}
     */
    @Select("select count(*) from sys_log t where t.username like '%${username}%' and t.type = #{type}")
    Integer findLogByUsername(@Param("username") String username, Integer type);

    /**
     * 发现日志页面通过用户名
     *
     * @param username 用户名
     * @param type     类型
     * @param page     页面
     * @param limit    限制
     * @return {@link List<SysLog>}
     */
    @Select("select * from sys_log t where t.username like '%${username}%' and t.type = #{type} limit #{page},#{limit}")
    List<SysLog> findLogByUsernameByPage(@Param("username") String username, Integer type, @Param("page") Integer page, @Param("limit") Integer limit);

}
