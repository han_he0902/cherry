package com.hanshg.cherry.mapper.log;

import com.hanshg.cherry.mapper.base.MyBaseMapper;
import com.hanshg.cherry.model.SysErrorLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName ErrorLogMapper
 * @Description 异常日志
 * @Author 柠檬水
 * @Date 2020/4/21 21:22
 * @Version 1.0
 **/
@Mapper
public interface ErrorLogMapper extends MyBaseMapper<SysErrorLog> {

}
