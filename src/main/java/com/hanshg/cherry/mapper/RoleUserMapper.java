package com.hanshg.cherry.mapper;

import com.hanshg.cherry.mapper.base.MyBaseMapper;
import com.hanshg.cherry.model.SysRoleUser;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 用户角色映射器
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@Mapper
public interface RoleUserMapper extends MyBaseMapper<SysRoleUser> {

    /**
     * 保存
     *
     * @param sysRoleUser 系统用户角色
     */
    @Insert("insert into sys_role_user(userId,roleId) " +
            "values(#{userId},#{roleId})")
    void save(SysRoleUser sysRoleUser);

    /**
     * sys角色用户的用户id
     *
     * @param userId 用户id
     * @return {@link SysRoleUser}
     */
    @Select("select * from sys_role_user t where t.userId = #{userId}")
    SysRoleUser getSysRoleUserByUserId(Long userId);

    /**
     * 更新系统用户角色
     *
     * @param sysRoleUser 系统用户角色
     * @return int
     */
    int updateSysRoleUser(SysRoleUser sysRoleUser);

    /**
     * 删除角色用户的用户id
     *
     * @param userId 用户id
     * @return int
     */
    @Delete("delete from sys_role_user t where t.userId = #{userId}")
    int deleteRoleUserByUserId(Long userId);

    /**
     * 列出所有系统用户角色id
     *
     * @param roleId 角色id
     * @return {@link List<SysRoleUser>}
     */
    @Select("select * from sys_role_user t where t.roleId = #{roleId}")
    List<SysRoleUser> listAllSysRoleUserByRoleId(Long roleId);
}
