package com.hanshg.cherry.mapper.sys;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * 数据类型映射器
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@Mapper
public interface DataTypeMapper {

    /**
     * 计算表
     *
     * @return {@link Integer}
     */
    @Select("select COUNT(*) from information_schema.tables where table_schema='cherry'")
    Integer countTable();

    /**
     * mysql版本
     *
     * @return {@link String}
     */
    @Select("select version()")
    String mysqlVersion();

    /**
     * 数据库
     *
     * @return {@link String}
     */
    @Select("select @@version_comment from dual")
    String dataBase();
}
