package com.hanshg.cherry.mapper.job;

import com.hanshg.cherry.mapper.base.MyBaseMapper;
import com.hanshg.cherry.model.SysJob;
import org.apache.ibatis.annotations.Mapper;

/**
 * 映射器工作
 *
 * @ClassName JobMapper
 * @Description TODO
 * @Author 柠檬水
 * @Date 2020/4/16 10:01
 * @Version 1.0
 **/
@Mapper
public interface JobMapper extends MyBaseMapper<SysJob> {

}
