package com.hanshg.cherry.mapper.crawler;

import com.hanshg.cherry.mapper.base.MyBaseMapper;
import com.hanshg.cherry.model.SysCrawler;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName CrawlerMapper
 * @Description TODO
 * @Author 柠檬水
 * @Date 2020/4/23 14:43
 * @Version 1.0
 **/
@Mapper
public interface CrawlerMapper extends MyBaseMapper<SysCrawler> {

}
