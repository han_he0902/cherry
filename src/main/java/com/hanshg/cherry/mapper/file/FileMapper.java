package com.hanshg.cherry.mapper.file;

import com.hanshg.cherry.mapper.base.MyBaseMapper;
import com.hanshg.cherry.model.SysFile;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 文件映射器
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@Mapper
public interface FileMapper extends MyBaseMapper<SysFile> {

    /**
     * 通过id获取文件
     *
     * @param id id
     * @return {@link SysFile}
     */
    @Select("select * from sys_file t where t.id = #{id}")
    SysFile getFileById(Long id);

    /**
     * 删除文件
     *
     * @param id id
     * @return {@link Integer}
     */
    @Delete("delete from sys_file where id = #{id}")
    Integer deleteFile(Long id);

    /**
     * 更新文件
     *
     * @param sysFile sys文件
     * @return {@link Integer}
     */
    Integer updateFile(SysFile sysFile);

    /**
     * 保存文件
     *
     * @param sysFile sys文件
     * @return {@link Integer}
     */
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into sys_file(title,fileName, size_value,pathUrl,visitUrl, status, create_time, update_time) values(#{title},#{fileName}, #{sizeValue},#{pathUrl},#{visitUrl}, #{status}, now(), now())")
    Integer saveFile(SysFile sysFile);

    /**
     * 计算文件
     *
     * @return {@link Integer}
     */
    @Select("select count(*) from sys_file t")
    Integer countFile();

    /**
     * 把所有文件页面
     *
     * @param page  页面
     * @param limit 限制
     * @return {@link List<SysFile>}
     */
    @Select("select * from sys_file t order by t.id limit #{page},#{limit} ")
    List<SysFile> getAllFileByPage(@Param("page") Integer page, @Param("limit") Integer limit);

    /**
     * 找到页面文件标题
     *
     * @param title 标题
     * @param page  页面
     * @param limit 限制
     * @return {@link List<SysFile>}
     */
    @Select("select * from sys_file t where t.title like '%${title}%' limit #{page},#{limit}")
    List<SysFile> findFileByTitleToPage(String title, Integer page, Integer limit);

    /**
     * 找到文件标题
     *
     * @param title 标题
     * @return {@link Integer}
     */
    @Select("select count(*) from sys_file t where t.title like '%${title}%'")
    Integer findFileByTitle(@Param("title") String title);
}
