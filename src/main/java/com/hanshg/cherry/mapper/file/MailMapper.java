package com.hanshg.cherry.mapper.file;

import com.hanshg.cherry.mapper.base.MyBaseMapper;
import com.hanshg.cherry.model.SysMail;
import org.apache.ibatis.annotations.Mapper;

/**
 * 邮件映射器
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@Mapper
public interface MailMapper extends MyBaseMapper<SysMail> {

}
