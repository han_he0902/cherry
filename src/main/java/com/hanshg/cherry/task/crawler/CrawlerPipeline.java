package com.hanshg.cherry.task.crawler;

import com.hanshg.cherry.model.SysCrawler;
import com.hanshg.cherry.service.crawler.CrawlerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

/**
 * @ClassName CrawlerPipeline
 * @Description TODO
 * @Author 柠檬水
 * @Date 2020/4/23 15:30
 * @Version 1.0
 **/
@Component
@Slf4j
public class CrawlerPipeline implements Pipeline {

    @Autowired
    private CrawlerService crawlerService;

    /**
     * Process extracted results.
     *
     * @param resultItems resultItems
     * @param task        task
     */
    @Override
    public void process(ResultItems resultItems, Task task) {
        SysCrawler crawler = resultItems.get("crawler");
        if (crawler != null){
            crawlerService.saveCrawler(crawler);
        }
    }
}
