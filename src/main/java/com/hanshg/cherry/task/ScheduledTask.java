package com.hanshg.cherry.task;

import com.hanshg.cherry.task.crawler.CrawlerJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @ClassName ScheduledTask
 * @Description 测试任务类
 * @Author 柠檬水
 * @Date 2020/4/20 16:28
 * @Version 1.0
 **/
@Component
@Slf4j
public class ScheduledTask {

    @Autowired
    private CrawlerJob crawlerJob;

    /**
     * 任务参数
     *
     * @param params 参数个数
     */
    public void taskWithParams(String params) {
        System.out.println("执行有参示例任务：" + params);
    }

    /**
     * 任务没有参数
     */
    public void taskNoParams() {
        System.out.println("执行无参示例任务");
    }

    /**
     * 开始抓取任务
     */
    public void startJob() {
        crawlerJob.startJob();
    }
}
