package com.hanshg.cherry.task.zztuku;

/**
 * @ClassName ConstantCode
 * @Description TODO
 * @Author 柠檬水
 * @Date 2020/4/26 22:52
 * @Version 1.0
 **/
public class ConstantCode {

    public static final String URL = "https://www.zztuku.com/member-signin.html";

    public static final String REFERER = "https://www.zztuku.com/Index-index.html";

    public static final String COOKIE = "zzTuKu_Com=qq3o0d93q4b6fceqhohkm2ud63; __guid=135588855.3012577651574594000.1587909296954.234; uid=BHNcNQc0Bj5XdVI9UmZdYVRnB2NXcQVq; monitor_count=3; BAIDU_SSP_lcr=https://graph.qq.com/oauth2.0/show?which=Login&display=pc&client_id=101393123&redirect_uri=https%3A%2F%2Fwww.zztuku.com%2FOauth-callback-type-qq.html&response_type=code&scope=get_user_info%2Cadd_share; Hm_lvt_36d091ca4167858e40c7bf39f954e3f9=1587909298,1587909381; Hm_lpvt_36d091ca4167858e40c7bf39f954e3f9=1587909381";

    public static final String X_REQUESTED_WITH = "XMLHttpRequest";

    public static final String USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36";
}
