package com.hanshg.cherry.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.mapper.client.ClientMapper;
import com.hanshg.cherry.model.SysClientMenu;
import com.hanshg.cherry.service.client.ClientService;
import com.hanshg.cherry.util.sys.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 柠檬水
 */
@Service
@Transactional
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientMapper clientMapper;

    @Override
    public List<SysClientMenu> getAll() {
        QueryWrapper<SysClientMenu> wrapper = new QueryWrapper();
        wrapper.eq("status",1).orderByAsc("sort");
        return clientMapper.selectList(wrapper);
    }

    @Override
    public Results<SysClientMenu> list() {
        QueryWrapper<SysClientMenu> wrapper = new QueryWrapper();
        wrapper.orderByAsc("sort");
        return Results.success(0,clientMapper.selectList(wrapper));
    }

    @Override
    public Integer deleteById(Long id) {
        return clientMapper.deleteById(id);
    }

    @Override
    public Results<SysClientMenu> update(SysClientMenu clientMenu) {
        Integer count = clientMapper.updateById(clientMenu);
        if (count > 0) {
            return Results.success();
        }
        return Results.failure();
    }

    @Override
    public Integer count() {
        return clientMapper.selectCount(null);
    }

    @Override
    public SysClientMenu getClientMenuById(Long id) {
        return clientMapper.selectById(id);
    }

    @Override
    public Results<SysClientMenu> save(SysClientMenu clientMenu) {
        Integer count = clientMapper.insert(clientMenu);
        if (count > 0) {
            return Results.success();
        }
        return Results.failure();
    }

    @Override
    public Results<SysClientMenu> recycleList() {
        return Results.success(0,clientMapper.recycleList());
    }

    @Override
    public Integer deleteRecycle(Long id) {
        return clientMapper.deletePeace(id);
    }

    @Override
    public Results reduceMenu(SysClientMenu clientMenu) {
        int i = clientMapper.reduceMenu(clientMenu.getId());
        if (i > 0) {
            return Results.success();
        }
        return Results.failure();
    }

    /**
     * 服务器信息
     *
     * @return {@link Server}
     */
    @Override
    public Server server() {
        Server server = new Server();
        try {
            server.copyTo();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return server;
    }
}
