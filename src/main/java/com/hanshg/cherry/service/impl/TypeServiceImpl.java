package com.hanshg.cherry.service.impl;

import com.hanshg.cherry.base.result.JSONResult;
import com.hanshg.cherry.mapper.wx.TypeMapper;
import com.hanshg.cherry.service.wx.TypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author 柠檬水
 */
@Service
@Transactional
public class  TypeServiceImpl implements TypeService {

    private final TypeMapper typeMapper;

    public TypeServiceImpl(TypeMapper typeMapper) {
        this.typeMapper = typeMapper;
    }

    @Override
    public JSONResult getNewsType() {
        return JSONResult.ok(typeMapper.selectList(null));
    }

}
