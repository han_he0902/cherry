package com.hanshg.cherry.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.dto.LoginUser;
import com.hanshg.cherry.mapper.file.MailInfoMapper;
import com.hanshg.cherry.model.SysMailInfo;
import com.hanshg.cherry.service.UserService;
import com.hanshg.cherry.service.mail.MailInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author 柠檬水
 */
@Service
@Transactional
@Slf4j
public class MailInfoServiceImpl implements MailInfoService {

    private final MailInfoMapper mailInfoMapper;

    private final UserService userService;

    public MailInfoServiceImpl(MailInfoMapper mailInfoMapper, UserService userService) {
        this.mailInfoMapper = mailInfoMapper;
        this.userService = userService;
    }

    @Override
    public Results<SysMailInfo> getMailInfoByPage(Integer offset, Integer limit) {
        Page<SysMailInfo> page = new Page<>(offset, limit);
        QueryWrapper<SysMailInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("operation",getUserName());
        wrapper.orderByDesc("create_time");
        IPage<SysMailInfo> productPage = mailInfoMapper.selectPage(page, wrapper);
        return Results.success((int) productPage.getTotal(), productPage.getRecords());
    }

    @Override
    public Results<SysMailInfo> getMailInfoByPage(Integer offset, Integer limit, SysMailInfo sysMailInfo) {
        Page<SysMailInfo> page = new Page<>(offset, limit);
        QueryWrapper<SysMailInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("operation",getUserName());
        wrapper.like("title",sysMailInfo.getTitle());
        wrapper.orderByDesc("create_time");
        IPage<SysMailInfo> productPage = mailInfoMapper.selectPage(page, wrapper);
        return Results.success((int) productPage.getTotal(), productPage.getRecords());
    }

    @Override
    public Results saveMailInfo(SysMailInfo sysMailInfo) {
        sysMailInfo.setOperation(getUserName());
        int i = mailInfoMapper.insert(sysMailInfo);
        if (i > 0) {
            return Results.success();
        }
        return Results.failure();
    }

    @Override
    public Results deleteMailInfo(Long id) {
        int i = mailInfoMapper.deleteById(id);
        if (i > 0) {
            return Results.success();
        }
        return Results.failure();
    }

    @Override
    public SysMailInfo getMailInfoById(Long id) {
        return mailInfoMapper.selectById(id);
    }

    @Override
    public Results<SysMailInfo> recycleList() {
        return Results.success(0,mailInfoMapper.recycleList(getUserName()));
    }

    @Override
    public Integer deleteRecycle(Long id) {
        return mailInfoMapper.deletePeace(id);
    }

    @Override
    public Results reduceProduct(SysMailInfo sysMailInfo) {
        int i = mailInfoMapper.reduceProduct(sysMailInfo.getId());
        if (i > 0) {
            return Results.success();
        }
        return Results.failure();
    }

    /**
     * 获取登录用户信息
     * @return
     */
    private String getUserName(){
        LoginUser loginUser = userService.getLoginUser();
        return loginUser.getUsername();
    }
}
