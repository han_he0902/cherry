package com.hanshg.cherry.service.impl;

import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.mapper.log.LogMapper;
import com.hanshg.cherry.model.SysLog;
import com.hanshg.cherry.service.log.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author 柠檬水
 */
@Service
@Transactional
public class LogServiceImpl implements LogService {

    @Autowired
    private LogMapper logMapper;

    @Override
    public Results<SysLog> getAllLogByPage(Integer type,Integer typeMenu, Integer page, Integer limit) {
        return Results.success(logMapper.countLog(type,typeMenu), logMapper.getAllLogByPage(type,typeMenu, page, limit));
    }

    @Override
    public Results<SysLog> findLogByUsernameByPage(String username,Integer type, Integer page, Integer limit) {
        return Results.success(logMapper.findLogByUsername(username, type), logMapper.findLogByUsernameByPage(username, type, page, limit));
    }

    @Override
    public Integer saveLog(SysLog sysLog) {
        return logMapper.saveLog(sysLog);
    }

}
