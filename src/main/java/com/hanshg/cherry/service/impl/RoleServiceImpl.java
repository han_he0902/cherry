package com.hanshg.cherry.service.impl;

import com.hanshg.cherry.base.result.ResponseCode;
import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.dto.RoleDto;
import com.hanshg.cherry.mapper.RoleMapper;
import com.hanshg.cherry.mapper.RolePermissionMapper;
import com.hanshg.cherry.mapper.RoleUserMapper;
import com.hanshg.cherry.model.SysRole;
import com.hanshg.cherry.model.SysRoleUser;
import com.hanshg.cherry.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

/**
 * @author 柠檬水
 */
@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    RolePermissionMapper rolePermissionMapper;

    @Autowired
    private RoleUserMapper roleUserMapper;

    @Override
    public Results<SysRole> getAllRoles() {
        return Results.success(50, roleMapper.getRoles());
    }

    @Override
    public Results<SysRole> getAllRolesByPage(Integer offset, Integer limit) {
        return Results.success(roleMapper.countAllRoles(), roleMapper.getAllRolesByPage(offset, limit));
    }

    @Override
    public Results<SysRole> save(RoleDto roleDto) {
        //1、先保存角色"
        roleMapper.saveRole(roleDto);
        List<Long> permissionIds = roleDto.getPermissionIds();
        //移除0,permission id是从1开始
        permissionIds.remove(0L);
        //2、保存角色对应的所有权限
        if (!CollectionUtils.isEmpty(permissionIds)) {
            rolePermissionMapper.save(roleDto.getId(), permissionIds);
        }
        return Results.success();
    }

    @Override
    public SysRole getRoleById(Long id) {
        return roleMapper.getById(id);
    }

    @Override
    public Results update(RoleDto roleDto) {
        List<Long> permissionIds = roleDto.getPermissionIds();
        permissionIds.remove(0L);
        //1、更新角色权限之前要删除该角色之前的所有权限
        rolePermissionMapper.deleteRolePermission(roleDto.getId());

        //2、判断该角色是否有赋予权限值，有就添加"
        if (!CollectionUtils.isEmpty(permissionIds)) {
            rolePermissionMapper.save(roleDto.getId(), permissionIds);
        }

        //3、更新角色表
        roleDto.setUpdateTime(new Date());
        int countData = roleMapper.updateRole(roleDto);

        if(countData > 0){
            return Results.success();
        }else{
            return Results.failure();
        }
    }

    @Override
    public Results delete(Long id) {
        List<SysRoleUser> datas = roleUserMapper.listAllSysRoleUserByRoleId(id);
        if(datas.size() <= 0){
            roleMapper.deleteRole(id);
            return Results.success();
        }
        return Results.failure(ResponseCode.USERNAME_REPEAT.USER_ROLE_NO_CLEAR.getCode(), ResponseCode.USERNAME_REPEAT.USER_ROLE_NO_CLEAR.getMessage());
    }

    @Override
    public Results<SysRole> getRoleByFuzzyRoleNamePage(String roleName, Integer startPosition, Integer limit) {
        return Results.success(roleMapper.countRoleByFuzzyRoleName(roleName), roleMapper.getRoleByFuzzyRoleNamePage(roleName,startPosition, limit));
    }
}
