package com.hanshg.cherry.service.impl;

import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.mapper.file.FileMapper;
import com.hanshg.cherry.model.SysFile;
import com.hanshg.cherry.service.file.FileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author 柠檬水
 */
@Service
@Transactional
@Slf4j
public class FileServiceImpl implements FileService {

    @Autowired
    private FileMapper fileMapper;

    @Override
    public Results<SysFile> getAllFileByPage(Integer offset, Integer limit) {
        return Results.success(fileMapper.countFile(), fileMapper.getAllFileByPage(offset, limit));
    }

    @Override
    public Results<SysFile> saveFile(SysFile sysFile) {
        Integer res = fileMapper.saveFile(sysFile);
        if (res > 0) {
            return Results.success();
        }
        return Results.failure();
    }

    @Override
    public Results getFileById(Long id) {
        return Results.success(fileMapper.getFileById(id));
    }

    @Override
    public SysFile getSysFileById(Long id) {
        return fileMapper.getFileById(id);
    }

    @Override
    public Results<SysFile> findFileByTitle(String title, Integer offset, Integer limit) {
        return Results.success(fileMapper.findFileByTitle(title), fileMapper.findFileByTitleToPage(title, offset, limit));
    }

    @Override
    public Results updateFile(SysFile sysFile) {
        sysFile.setUpdateTime(new Date());
        int res = fileMapper.updateFile(sysFile);
        if (res > 0) {
            return Results.success();
        }
        return Results.failure();
    }

    @Override
    public Results deleteFile(Long id) {
        int res = fileMapper.deleteFile(id);
        if (res > 0) {
            return Results.success();
        }
        return Results.failure();
    }

}
