package com.hanshg.cherry.service.file;

import com.hanshg.cherry.model.SysFile;
import com.hanshg.cherry.base.result.Results;

public interface FileService {

    Results<SysFile> getAllFileByPage(Integer offset, Integer limit);

    Results<SysFile> saveFile(SysFile sysFile);

    Results<SysFile> getFileById(Long id);

    Results<SysFile> updateFile(SysFile sysFile);

    Results<SysFile> deleteFile(Long id);

    SysFile getSysFileById(Long id);

    Results<SysFile> findFileByTitle(String title, Integer offset, Integer limit);
}

