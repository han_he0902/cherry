package com.hanshg.cherry.service;

import com.hanshg.cherry.base.result.Results;

/**
 * @author 柠檬水
 */
public interface RoleUserService {
    /**
     * 获取对象
     * @param userId
     * @return
     */
    Results getSysRoleUserByUserId(Long userId);
}
