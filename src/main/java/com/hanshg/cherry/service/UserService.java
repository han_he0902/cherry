package com.hanshg.cherry.service;

import com.alibaba.fastjson.JSONObject;
import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.dto.LoginUser;
import com.hanshg.cherry.dto.UserDto;
import com.hanshg.cherry.model.SysUser;

/**
 * 用户服务
 *
 * @author 柠檬水
 * @date 2020/04/14
 */
public interface UserService {

    /**
     * 获取用户
     *
     * @param username 用户名
     * @return {@link SysUser}
     */
    SysUser getUser(String username);

    /**
     * 得到用户的id
     *
     * @param id id
     * @return {@link SysUser}
     */
    SysUser getUserById(Long id);

    /**
     * 删除用户
     *
     * @param id id
     * @return int
     */
    int deleteUser(Long id);

    /**
     * 保存用户
     *
     * @param sysUser 系统用户
     * @return int
     */
    int saveUser(SysUser sysUser);

    /**
     * 更新用户
     *
     * @param sysUser 系统用户
     * @return int
     */
    int updateUser(SysUser sysUser);

    /**
     * 得到所有用户的页面
     *
     * @param offset 抵消
     * @param limit  限制
     * @return {@link Results<SysUser>}
     */
    Results<SysUser> getAllUserByPage(Integer offset, Integer limit);

    /**
     * 保存
     *
     * @param sysUser 系统用户
     * @param roleId  角色id
     * @return {@link Results<SysUser>}
     */
    Results<SysUser> save(SysUser sysUser, Long roleId);

    /**
     * 获取用户通过电话
     *
     * @param phone 电话
     * @return {@link SysUser}
     */
    SysUser getUserByPhone(String phone);

    /**
     * 获取用户的用户名
     *
     * @param username 用户名
     * @return {@link SysUser}
     */
    SysUser getUserByUserName(String username);

    /**
     * 让用户通过电子邮件
     *
     * @param email 电子邮件
     * @return {@link SysUser}
     */
    SysUser getUserByEmail(String email);

    /**
     * 更新用户
     *
     * @param userDto 用户dto
     * @param roleId  角色id
     * @return {@link Results<SysUser>}
     */
    Results<SysUser> updateUser(UserDto userDto, Long roleId);

    /**
     * 找到用户的用户名
     *
     * @param username 用户名
     * @param offset   抵消
     * @param limit    限制
     * @return {@link Results<SysUser>}
     */
    Results<SysUser> findUserByUsername(String username, Integer offset, Integer limit);

    /**
     * 更改密码
     *
     * @param username    用户名
     * @param oldPassword 旧密码
     * @param newPassword 新密码
     * @return {@link Results<SysUser>}
     */
    Results<SysUser> changePassword(String username, String oldPassword, String newPassword);

    /**
     * 获取登录用户
     *
     * @return {@link LoginUser}
     */
    LoginUser getLoginUser();

    /**
     * 发送短信
     * @param mobile
     */
    JSONObject sendSms(String mobile);
}
