package com.hanshg.cherry.service.log;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hanshg.cherry.model.SysErrorLog;

public interface ErrorLogService extends IService<SysErrorLog> {

}
