package com.hanshg.cherry.service.wx;


import com.hanshg.cherry.base.result.JSONResult;

/**
 * @author 柠檬水
 */
public interface TypeService {

   /**
    * 获取所有类型列表
    * @return
    */
   JSONResult getNewsType();

}

