package com.hanshg.cherry.service.aliyun;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.oss.model.OSSObjectSummary;
import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.dto.OssObjectDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * 阿里云操作系统服务
 *
 * @author 柠檬水
 * @date 2020/04/14
 */
public interface AliYunOssService {

    /**
     * 回家上传图像oss
     * 图片 上传阿里云oss
     *
     * @param file 文件
     * @return {@link String}
     * @throws Exception 异常
     */
    String uploadHomeImageOSS(MultipartFile file) throws Exception;

    /**
     * 获得图片路径
     *
     * @param fileUrl
     * @return
     */
    String getHomeImageUrl(String fileUrl);

    /**
     * 图片上传到OSS服务器  如果同名文件会覆盖服务器上的
     *
     * @param inseam 文件流
     * @param fileName 文件名称 包括后缀名
     * @return 出错返回"" ,唯一MD5数字签名
     */
    String uploadHomeImageFileOSS(InputStream inseam, String fileName);

    /**
     * 获得url链接
     *
     * @param key
     * @return
     */
    String getUrl(String key);

    /**
     * getcontent类型
     * 判断OSS服务文件上传时文件的类型contentType
     *
     * @param FilenameExtension 文件后缀
     * @return String
     */
    String getcontentType(String FilenameExtension);

    /**
     * 上传文件
     *
     * @param file
     * @return
     * @throws Exception
     */
    JSONObject updateHomeImage(MultipartFile file) throws Exception;

    /**
     * 文件列表
     * 获取200限量文件
     *
     * @return {@link List<OSSObjectSummary>}
     */
    Results<OssObjectDto> fileList();

    /**
     * 删除文件
     *
     * @param fileName 对象名称
     * @return {@link JSONObject}
     */
    JSONObject deleteFile(List<String> fileName);

    /**
     * 下载文件
     * @param os
     * @param objectName
     * @throws IOException
     */
    void exportOssFile(OutputStream os, String objectName) throws IOException;
}
