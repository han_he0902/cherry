package com.hanshg.cherry.service.aliyun;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hanshg.cherry.model.SysOss;

/**
 * @ClassName OssService
 * @Description TODO
 * @Author 柠檬水
 * @Date 2020/4/16 10:10
 * @Version 1.0
 **/
public interface OssService extends IService<SysOss> {

    /**
     * 得到系统oss
     *
     * @return {@link SysOss}
     */
    SysOss getSysOss();
}
