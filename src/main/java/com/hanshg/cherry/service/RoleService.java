package com.hanshg.cherry.service;

import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.dto.RoleDto;
import com.hanshg.cherry.model.SysRole;

public interface RoleService {

    Results<SysRole> getAllRoles();

    Results<SysRole> getAllRolesByPage(Integer offset, Integer limit);

    Results<SysRole> save(RoleDto roleDto);

    SysRole getRoleById(Long id);

    Results update(RoleDto roleDto);

    Results delete(Long id);

    Results<SysRole> getRoleByFuzzyRoleNamePage(String roleName, Integer offset, Integer limit);
}
