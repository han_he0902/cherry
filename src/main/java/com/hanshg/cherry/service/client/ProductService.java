package com.hanshg.cherry.service.client;

import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.model.SysClientProduct;

import java.util.List;

public interface ProductService {

    Results<SysClientProduct> getProductByPage(Integer offset, Integer limit);

    Results<SysClientProduct> getProductByPage(Integer offset, Integer limit, SysClientProduct clientProduct);

    Results saveProduct(SysClientProduct clientProduct);

    Results<SysClientProduct> getProductById(Long id);

    Results updateProduct(SysClientProduct clientProduct);

    Results deleteProduct(Long id);

    SysClientProduct getSysClientProductById(Long id);

    Results<SysClientProduct> recycleList();

    Integer deleteRecycle(Long id);

    Results reduceProduct(SysClientProduct clientProduct);

    List<SysClientProduct> allProduct();
}

