package com.hanshg.cherry.service.client;


import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.model.SysClientMenu;
import com.hanshg.cherry.util.sys.Server;

import java.util.List;

public interface ClientService {

    List<SysClientMenu> getAll();

    Results<SysClientMenu> list();

    Integer deleteById(Long id);

    Results<SysClientMenu> update(SysClientMenu clientMenu);

    Integer count();

    SysClientMenu getClientMenuById(Long id);

    Results<SysClientMenu> save(SysClientMenu clientMenu);

    Results<SysClientMenu> recycleList();

    Integer deleteRecycle(Long id);

    Results reduceMenu(SysClientMenu clientMenu);

    Server server();
}
