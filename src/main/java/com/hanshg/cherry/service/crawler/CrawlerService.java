package com.hanshg.cherry.service.crawler;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hanshg.cherry.model.SysCrawler;

public interface CrawlerService extends IService<SysCrawler> {

    boolean saveCrawler(SysCrawler crawler);
}