package com.hanshg.cherry.service.mail;


import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.model.SysMailInfo;

/**
 * @author 柠檬水
 */
public interface MailInfoService {

    /**
     * 分页查询
     * @param offset
     * @param limit
     * @return
     */
    Results<SysMailInfo> getMailInfoByPage(Integer offset, Integer limit);

    /**
     * 模糊查询
     * @param offset
     * @param limit
     * @param sysMailInfo
     * @return
     */
    Results<SysMailInfo> getMailInfoByPage(Integer offset, Integer limit, SysMailInfo sysMailInfo);

    /**
     * 保存
     * @param sysMailInfo
     * @return
     */
    Results saveMailInfo(SysMailInfo sysMailInfo);

    /**
     * 删除
     * @param id
     * @return
     */
    Results deleteMailInfo(Long id);

    /**
     * 查询单个对象
     * @param id
     * @return
     */
    SysMailInfo getMailInfoById(Long id);

    /**
     * 回收列表
     *
     * @return {@link Results<SysMailInfo>}
     */
    Results<SysMailInfo> recycleList();

    /**
     * 删除回收
     *
     * @param id id
     * @return {@link Integer}
     */
    Integer deleteRecycle(Long id);

    /**
     * 减少产品
     *
     * @param sysMailInfo 系统邮件信息
     * @return {@link Results}
     */
    Results reduceProduct(SysMailInfo sysMailInfo);
}
