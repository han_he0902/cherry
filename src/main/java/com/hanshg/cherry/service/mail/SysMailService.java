package com.hanshg.cherry.service.mail;

import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.model.SysMail;

/**
 * @author 柠檬水
 */
public interface SysMailService {

    /**
     * 获取单个对象
     * @return
     */
    SysMail getSysMail();

    /**
     * 保存
     * @param sysMail
     * @return
     */
    Results saveMail(SysMail sysMail);

    /**
     * 更新
     * @param sysMail
     * @return
     */
    Results updateMail(SysMail sysMail);

}
