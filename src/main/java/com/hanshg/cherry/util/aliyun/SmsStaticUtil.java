package com.hanshg.cherry.util.aliyun;

/**
 * @ClassName SmsStaticUtil
 * @Description 短信静态属性类
 * @Author 柠檬水
 * @Date 2020/4/18 16:19
 * @Version 1.0
 **/
public class SmsStaticUtil {

    /**
     * 设置超时时间-可自行调整
     */
    public static final String defaultConnectTimeout = "sun.net.client.defaultConnectTimeout";
    public static final String defaultReadTimeout = "sun.net.client.defaultReadTimeout";
    public static final Integer Timeout = 10000;

    public static final String SendSms = "SendSms";

    public static final String QuerySendDetails = "QuerySendDetails";
    /**
     * 短信API产品域名（接口地址固定，无需修改）
     */
    public static final String domain = "dysmsapi.aliyuncs.com";

}
