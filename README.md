#  Cherry 柠檬水

###  项目介绍

* Cherry柠檬水是基于StringBoot2.2+Layui+Mybatis+Mybatis-Plus开发的一套轻量级的权限管理系统，开箱即用。

* 提供了代码生成器，可快速完成开发任务
* 完善的XSS防范及脚本过滤，彻底杜绝XSS攻击
* 支持MySQL、Oracle、SQL Server等主流数据库

###  技术选型

* 核心框架：Spring Boot 2.2.5
* 安全框架：Spring Security 5.2.2
* 视图框架：Spring MVC 5.2.4
* 持久层框架：MyBatis 2.1.2  MyBatis-Plus 3.3.2
* 数据库连接池：Druid 1.1.20
* 缓存框架： Redis 2.2.5
* 日志管理：SLF4J 2.12.1、Logback 1.2.3
* 模板引擎：Thymeleaf 3.0.11
* 页面交互：Layui 2.5.6

### 其他框架

* 爬虫框架：webmagic

###  软件需求

* JDK 1.8
* MySQL 8.0+
* Maven 3.5+

###  内置功能
1. 登录：登录入口，验证码登录
2. 注册：用户注册入口，短信验证码注册
3. 用户管理：用户是系统操作者，该功能主要完成系统用户配置。
4. 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5. 角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6. 系统设置：配置邮件系统、阿里云OSS存储等参数。
7. 文件系统：管理上传的附件信息、阿里云OSS存储数据。
8. 邮件系统：发送简单、复杂邮件，包含：附件、富文本编辑器，对发件箱管理。
9. 操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
12. 系统接口：根据业务代码自动生成相关的api接口文档。
13. 服务监控：定时任务设置、监视当前系统CPU、内存、磁盘、堆栈等相关信息。
14. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。
15. 图标字体：查看系统、layui、awesome图标及使用方式。

###  本地部署

- 通过gitee下载源码
- idea、eclipse需安装lombok插件，不然会提示找不到entity的get set方法
- 创建数据库cherry，数据库字符集为utf8mb4 -- UTF-8 Unicode
- 执行db/Cherry.sql文件，初始化数据【按需导入表结构及数据】
- 修改application.yml文件，更新MySQL账号和密码，更新Redis、阿里云keyId....
- Eclipse、IDEA运行CherryApplication.java，则可启动项目
- Cherry柠檬水访问路径：[http://localhost:9001]

### 在线预览
* [Cherry柠檬水在线预览](http://www.cherryok.cn)

* 测试账户：cherry/cherry

### 演示图

![登录](https://hanshg-oss.oss-cn-beijing.aliyuncs.com/system/%E7%99%BB%E5%BD%95.png "登录")

![注册](https://hanshg-oss.oss-cn-beijing.aliyuncs.com/system/%E6%B3%A8%E5%86%8C.png "注册")

![首页](https://hanshg-oss.oss-cn-beijing.aliyuncs.com/system/%E9%A6%96%E9%A1%B5.png "首页")

![权限管理](https://hanshg-oss.oss-cn-beijing.aliyuncs.com/system/%E6%9D%83%E9%99%90%E7%AE%A1%E7%90%86.png "权限管理")

![角色管理](https://hanshg-oss.oss-cn-beijing.aliyuncs.com/system/%E8%A7%92%E8%89%B2%E7%AE%A1%E7%90%86.png "角色管理")

![用户管理](https://hanshg-oss.oss-cn-beijing.aliyuncs.com/system/%E7%94%A8%E6%88%B7%E7%AE%A1%E7%90%86.png "用户管理")

![发送邮件](https://hanshg-oss.oss-cn-beijing.aliyuncs.com/system/%E9%82%AE%E4%BB%B6%E7%B3%BB%E7%BB%9F.png "发送邮件")

![定时任务](https://hanshg-oss.oss-cn-beijing.aliyuncs.com/system/%E5%AE%9A%E6%97%B6%E4%BB%BB%E5%8A%A1.png "定时任务")

![产品管理](https://hanshg-oss.oss-cn-beijing.aliyuncs.com/system/%E4%BA%A7%E5%93%81%E8%AE%BE%E7%BD%AE.png "产品管理")

![存储管理OSS](https://hanshg-oss.oss-cn-beijing.aliyuncs.com/system/%E5%AD%98%E5%82%A8%E7%AE%A1%E7%90%86OSS.png "存储管理OSS")

![接口API](https://hanshg-oss.oss-cn-beijing.aliyuncs.com/system/api.png "接口API")

![登录日志](https://hanshg-oss.oss-cn-beijing.aliyuncs.com/system/%E7%99%BB%E5%BD%95%E6%97%A5%E5%BF%97.png "登录日志")

![异常日志](https://hanshg-oss.oss-cn-beijing.aliyuncs.com/system/%E5%BC%82%E5%B8%B8%E6%97%A5%E5%BF%97.png "异常日志")



### 分享交流

* **开发者：柠檬水**
* **沟通交流：cherryok@126.com**
